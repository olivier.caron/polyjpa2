package jpa.entites;

import jakarta.persistence.*;

@Table(name="t_logiciel")
@Entity public class Logiciel  implements java.io.Serializable  {

	 private static final long serialVersionUID = 1L;
	 @Column(name="t_nom", length=40, unique=true, nullable=false) private String nom ;
	 @Id @Column (name="code") private int codeLogiciel ;
	 
	
	public Logiciel() {}
	
	public Logiciel(String nom, int code) {
		this.nom=nom ;
		this.codeLogiciel=code ;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	 
	public int getCodeLogiciel() {
		return codeLogiciel;
	}

	public void setCodeLogiciel(int codeLogiciel) {
		this.codeLogiciel = codeLogiciel;
	}


}