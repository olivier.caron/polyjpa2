package client;



import java.util.Collection;

import jpa.entites.Installation;
import jpa.entites.Ordinateur;
import jpa.services.GestionParc;


public class Main {

	public static void main(String[] args) {
		GestionParc gp=new GestionParc() ;
		
		int code=-1 ;
		
		gp.getT().begin() ;
		code=gp.testGeneratedValue() ;
		gp.getT().commit() ;
		System.out.println("code: "+code) ;
	
		int codeOrdi=-1;
		gp.getT().begin() ;
		codeOrdi=gp.initParc() ;
		gp.getT().commit() ;
		
		gp.getT().begin() ;
		Collection<Ordinateur> ordis = gp.testHierarchie() ;
		for (Ordinateur o : ordis) System.out.println(o.getNom());
		gp.getT().commit() ;
		
		gp.getT().begin() ;
		for (Installation i : gp.testOneToMany(codeOrdi)) System.out.println(i.getCodeInstall());
		gp.getT().commit() ;
	}
}
